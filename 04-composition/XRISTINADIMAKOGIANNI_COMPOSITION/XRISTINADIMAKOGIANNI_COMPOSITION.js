const message = '#HEALTH ALERT';
const messageX = 20;
const messageY = 150;
let img;
let img2;
let img3;
let img4;
let song;
let song2;
let song3;
let song4;
let song5;



function setup() {
  createCanvas(windowWidth, windowHeight);
  textSize(32);
//  img = loadImage ('data/HEALTH.jpg');
//  img2 = loadImage ('data/AIRPORT.jpg');
//  img3 = loadImage ('data/ATHENS.jpg');
//  img4 = loadImage ('data/SYNTAGMA.jpg');
  song = loadSound ('data/ENA.wav');
  song2 = loadSound ('data/MITSO2.wav');
  song3 = loadSound ('data/TSIODRAS1.wav');
  song4 = loadSound ('data/TSIODRAS2.wav');
  song5 = loadSound ('data/TSIODRASTRIA.wav');
}

function loaded() {
  song.pause;
  song2.pause;
  song3.pause;
  song4.pause;
  song5.pause;
  
}

function preload() {
  img = loadImage ('data/HEALTH.jpg');
  img2 = loadImage ('data/AIRPORT.jpg');
  img3 = loadImage ('data/ATHENS.jpg');
  img4 = loadImage ('data/SYNTAGMA.jpg');
}
  

function draw() {
//  background(32);
  if (isMouseInsideText(message, messageX, messageY)) {
    cursor(HAND);
    fill(0, 150, 200);
    stroke(0, 200, 255);
  } else {
    cursor(ARROW);
    fill(100);
    stroke(200);
  }
  text(message, messageX, messageY);
}

//  background(0, 0, 0);
//  let imgRatio = img.width/img.height;
//  let img2Ratio = img2.width/img2.height;
//  let img3Ratio = img3.width/img3.height;
//  let img4Ratio = img4.width/img4.height;
 
//}

function mouseClicked() {
  if (isMouseInsideText(message, messageX, messageY)) {
    window.open('https://lab.imedd.org/covid19/', '_blank');
  }
}

function isMouseInsideText(message, messageX, messageY) {
  const messageWidth = textWidth(message);
  const messageTop = messageY - textAscent();
  const messageBottom = messageY + textDescent();

  return mouseX > messageX && mouseX < messageX + messageWidth &&
    mouseY > messageTop && mouseY < messageBottom;
}

function keyPressed(){
  
  background(0, 0, 0);
  song.stop();
  song2.stop();
  song3.stop();
  song4.stop();
  song5.stop();

  if (key==="1"){
    imageMode(CENTER);
     image(img, width/2, height/2);
     song5.play();
     song5.loop();
  }

  if (key==="2") {
    imageMode(CENTER);
     image(img2, width/2, height/2);
     song.play();
     song.loop();
  }

  if (key==="3") {
    imageMode(CENTER);
     image(img3, width/2, height/2);
     song3.play();
     song3.loop();
  }

  if (key==="5") {
    imageMode(CENTER);
     image(img4, width/2, height/2);
     song2.play();
     song2.loop();
     
  }
}


//function mousePressed() {
//  let fs = fullscreen();
//  fullscreen(!fs);
//}

//function windowResized(){
//  resizeCanvas(windowWidth, windowHeight);
//}
