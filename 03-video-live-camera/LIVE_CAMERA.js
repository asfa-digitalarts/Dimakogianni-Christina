let video;
let button;

function setup() {
  createCanvas(320,240);
  background(51);
  video = createCapture(VIDEO); 
  video.size(320, 240);
  button = createButton('Click me'); 
  button.mousePressed(takesnap);

}

function takesnap() {
  image(video, 0, 0);
  textSize(50);
  text('LOCKDOWN', 15, 115);

}
